   /*     Programy są trzy. Można za nie otrzymać odpowiednio do 50, 50 i 25(75) punktów. Samodzielnie zdecyduj, które/ile programów chcesz napisać. Jeśli wybierzesz mniej niż jeden, nie będziemy mogli zaproponować Ci udziału w HoT#3 ;-)
        2. Szachownica (maksymalnie 50 punktów)

        Narysuj (wypisz) na standardowym wyjściu szachownicę z gwiazdek.
                Program powinien przyjmować jako parametry z command line oddzielone spacjami kolejno następujące wartości:
        szerokość kafelka
        wysokość kafelka
        liczba kafelków w poziomie
        liczba kafelków w pionie
        znak kafelka "białego" (pojedynczy)
        znak kafelka "czarnego" (pojedynczy)

        Wymagania dodatkowe:
        * program zawsze powinien zakończyć się z kodem 0, nawet jeśli parametry są błędne (np. brak parametru, dwa znaki zamiast jednego, itp.)
        * liczby mniejsze niż 1 lub większe niż 15 są niepoprawne
        * w przypadku błędnych danych wejściowych program nie powinien nic wypisywać na standardowym wyjściu. Nic.
        * kafelek "biały" powinien zawsze znajdować się w lewym górnym rogu
        * czas działania programu nie powinien przekraczać 30 sekund. Jeśli program będzie wykonywał się dłużej niż 30 sekund, zostanie przerwany przez system sprawdzający i nie będzie punktu za ten przypadek testowy
        * ostatni wiersz zawierający znaki rozwiązania powinien kończyć się znakiem przejścia do kolejnej linii i to powinien być ostatni znak wypisywany przez program
        * plik z programem powinien nazywać się Szachownica.java

        Przykładowe wyniki działania programu dla parametrów: 2 3 4 5 x * znajdziesz w załączonym pliku szachownica.png
*/
public class Szachownica {
    private static final byte NUMBEROFVALUES = 6;
    private byte widthOfTile;        //szerokosc kafelka
    private byte heightOfTile;       //wysokosc kafelka
    private byte tilesHorizontally;  //kafelki w poziomie
    private byte tilesVertically;    //kafelki w pionie
    private char white;              //char wypelnienia dla bialego kafelka
    private char black;              //char wypelnienia dla czarnego kafelka
    private char temp;               //char tymczasowy
    private String[] parts;          //tablica czesci inputa


    private Szachownica(String[] parts) {
        if (parts.length != NUMBEROFVALUES) {
            System.exit(0);
        }
        this.parts = parts;
        try {
            widthOfTile = Byte.parseByte(parts[0]);
            heightOfTile = Byte.parseByte(parts[1]);
            tilesHorizontally = Byte.parseByte(parts[2]);
            tilesVertically = Byte.parseByte(parts[3]);
        } catch (NumberFormatException e) {
            System.exit(0);
        }
        white = ' ';
        black = ' ';
        temp = ' ';
    }

    //sprawdza czy wartosci mieszcza sie w podanych zakresach
    private void checkValues() {
        if (widthOfTile > 15 || widthOfTile < 1 || heightOfTile > 15 || heightOfTile < 1 ||
                tilesHorizontally > 15 || tilesHorizontally < 1 || tilesVertically > 15 || tilesVertically < 1) {
            System.exit(0);
        }
    }

    //sprawdza i "parsuje" chary, ustawia unikalny temp
    private void checkChars() {
        //sprawdza czy propozycje charow sa dluzsze niz jeden znak
        if (parts[4].length() > 1 || parts[5].length() > 1) {
            System.exit(0);
        }

        white = parts[4].charAt(0);
        black = parts[5].charAt(0);

        //ustawia temp, ktory zawsze bedzie inny od white i black
        if (white != 'z' && black != 'z') {
            temp = 'z';
        } else if (white != 'x' && black != 'x') {
            temp = 'x';
        } else {
            temp = 'c';
        }
    }

    //pojedyncza linia kafelka bialego
    private String drawWhiteLine() {
        StringBuilder sBWhite = new StringBuilder();
        for (byte i = 0; i < widthOfTile; i++) {
            sBWhite.append(white);
        }
        return sBWhite.toString();
    }

    //pojedyncza linia kafelka czarnego
    private String drawBlackLine() {
        StringBuilder sBBlack = new StringBuilder();
        for (byte i = 0; i < widthOfTile; i++) {
            sBBlack.append(black);
        }
        return sBBlack.toString();
    }

    //rysowanie calego pierwszego rzedu znakow dla calej szachownicy
    private String drawHorizontalLine(String lineW, String lineB) {
        StringBuilder sBHorizontalLine = new StringBuilder();
        for (byte i = 0; i < tilesHorizontally; i++) {
            if (i % 2 == 0) {
                sBHorizontalLine.append(lineW);
            } else {
                sBHorizontalLine.append(lineB);
            }
        }
        return sBHorizontalLine.toString();
    }

    //rysowanie pierwszego rzedu calych kafelkow
    private String drawOneRowOfTiles(String horizontalLine) {
        StringBuilder sBRowOfTiles = new StringBuilder();
        for (byte i = 0; i < heightOfTile; i++) {
            sBRowOfTiles.append(horizontalLine + "\n");
        }
        return sBRowOfTiles.toString();
    }

    //odwrocenie dla rzedow parzystych
    private String drawReversedRowOfTiles(String rowOfTiles) {
        return rowOfTiles.replace(white, temp).replace(black, white).replace(temp, black);
    }

    //narysowanie szachownicy
    private void drawChessboard(String rowOfTiles, String reversedRowOfTiles) {
        for (byte i = 0; i < tilesVertically; i++) {
            if (i % 2 == 0) {
                System.out.print(rowOfTiles);
            } else {
                System.out.print(reversedRowOfTiles);
            }
        }
    }

    public static void main(String[] args) {
        Szachownica chessboard = new Szachownica(args);

        //walidacje wartosci i charow
        chessboard.checkValues();
        chessboard.checkChars();

        //Stopniowe tworzenie calych rzedow szachownicy
        String whiteLine = chessboard.drawWhiteLine();
        String blackLine = chessboard.drawBlackLine();
        String horizontalLine = chessboard.drawHorizontalLine(whiteLine, blackLine);
        String rowOfTiles = chessboard.drawOneRowOfTiles(horizontalLine);
        String reversedRowOfTiles = chessboard.drawReversedRowOfTiles(rowOfTiles);

        //rysowanie calej szachownicy
        chessboard.drawChessboard(rowOfTiles, reversedRowOfTiles);
    }
}