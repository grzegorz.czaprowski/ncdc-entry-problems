/*
Programy są trzy. Można za nie otrzymać odpowiednio do 50, 50 i 25(75) punktów. Samodzielnie zdecyduj, które/ile programów chcesz napisać. Jeśli wybierzesz mniej niż jeden, nie będziemy mogli zaproponować Ci udziału w HoT#3 ;-)

        3. Kosmolot (maksymalnie 25 punktów lub maksymalnie 75 punktów)

        Niedawno były święta. Zając Zbyszek stwierdził, że na Wielkanoc to on nie chce choinki (bo to nie te święta), tylko rakietę kosmiczną. Pomóż zajączkowi Zbyszkowi!

        Używając wskazanych poniżej znaków narysuj (wypisz) na standardowym wyjściu rakietę kosmiczną o rozmiarze n.

        Program powinien przyjmować jako parametry z command line oddzielone spacjami kolejno następujące wartości:
        rozmiar rakiety
        shield_on)

        Wymagania dodatkowe:
        * rozmiar rakiety może przyjmować wartości numeryczne od 1 do 75
        * shield_on może przyjmować wartości Y lub N (odpowiednio dla włączonego lub wyłączonego pancerza)
        * w przypadku błędnych danych wejściowych (np. brak parametru, dwa znaki zamiast jednego, itp.) program nie powinien nic wypisywać na standardowym wyjściu. Nic.
        * program zawsze powinien zakończyć się z kodem 0
        * czas działania programu nie powinien przekraczać 30 sekund. Jeśli program będzie wykonywał się dłużej niż 30 sekund, zostanie przerwany przez system sprawdzający i nie będzie punktu za ten przypadek testowy
        * ostatni wiersz zawierający znaki rozwiązania powinien kończyć się znakiem przejścia do kolejnej linii i to powinien być ostatni znak wypisywany przez program
        * plik z programem powinien nazywać się Kosmolot.java

        Lis Kleofas zasugerował, że z każdym kolejnym rozmiarem rakieta powinna rosnąć przynajmniej równie szybko jak rodzina zająca Zbyszka, więc przykładowe działanie dla pierwszych trzech rozmiarów rakiety powinno być zgodne z przykładami zapisanymi w pliku rakieta1.png

        Wszyscy w lesie wspierali zająca Zbyszka w realizacji jego marzeń i dlatego też nawet leciwa i znana z ostrożności klempa Leokadia wysiliła swoją pamięć i przypomniała sobie o czyhających w kosmosie zagrożeniach. Aby ochronić rakietę przed deszczem meteorytów zaproponowała zainstalowanie pancerzy ochronnych na bokach rakiety, ostrego szczytu oraz podobnie wyglądających, choć spełniających inną rolę silników jonowych na końcu. Jak wyglądać ma rakieta z wyłączonym pancerzem wiesz już z poprzednich przykładów. Zmodyfikowany projekt przykładowych trzech pierwszych rozmiarów rakiety z włączonym pancerzem możesz zobaczyć w załączonym pliku rakieta2.png

        Za przygotowanie kodu dla rakiety bez pancerza możesz otrzymać maksymalnie 25 punktów. Jeśli kod będzie umożliwiał dodatkowo generowanie rakiet z pancerzem, możesz otrzymać łącznie maksymalnie 75 punktów.
*/


public class Kosmolot {
    private static final byte NUMBEROFVALUES = 2;
    private byte size;
    private char star;
    private char space;
    private char engineAndTip;
    private char shield;
    private char shield2;
    private byte countOfSpaces;
    private String[] parts;
    private boolean isShieldON;
    private StringBuilder wing;
    private StringBuilder line;


    private Kosmolot(String[] parts) {
        if (parts.length != NUMBEROFVALUES) {
            System.exit(0);
        }
        this.parts = parts;
        star = '*';
        space = ' ';
        engineAndTip = '>';
        shield = '\\';
        shield2 = '/';
        isShieldON = false;
        wing = new StringBuilder();
        line = new StringBuilder();
        try {
            size = Byte.parseByte(parts[0]);
        } catch (NumberFormatException e) {
            System.exit(0);
        }
        countOfSpaces = size;
    }

    //sprawdza czy wartosci mieszcza sie w wymaganych zakresach
    private void checkValues() {
        if (size > 75 || size < 1) {
            System.exit(0);
        }
    }

    //Sprawdza czy zostało wprowadzone Y lub N i ustawia dla nich wartosc boolean
    private void checkChar() {
        if (parts[1].equals("Y")) {
            isShieldON = true;
        } else if (parts[1].equals("N")) {
            isShieldON = false;
        } else {
            System.exit(0);
        }
    }

    //for ktory rysuje pojedyncza, size - znakową czesc skrzydla dla "lewej" czesci statku
    private StringBuilder drawTopWing(StringBuilder wing) {
        for (byte i = 1; i < countOfSpaces; i++) {
            wing.append(space);
        }
        return wing;
    }

    //for ktory rysuje pojedyncza, size - znakową czesc skrzydla dla "prawej" czesci statku
    private StringBuilder drawBotWing(StringBuilder wing) {
        for (byte i = 0; i < countOfSpaces; i++) {
            wing.append(space);
        }
        return wing;
    }

    //for ktory dodaje do siebie skrzydlo size razy dla danego rzedu
    private StringBuilder drawLine(StringBuilder wing, StringBuilder line) {
        for (byte i = 0; i < size; i++) {
            line.append(wing);
        }
        return line;
    }

    private String ShieldTop(byte i, StringBuilder line) {
        if (isShieldON) {
            line.setCharAt(0, engineAndTip);
            if ((i != size - 1) && (i != 0)) {          //rysowanie tarcz
                for (byte z = 0; z < size; z++) {
                    line.setCharAt(z * size + i, shield);
                }
            } else if (i == 0) {             //rysowanie brzegu statku, by nie nadpisac skrajnego silnika
                for (byte z = 1; z < size; z++) {
                    line.setCharAt(z * size + i, shield);
                }
            } else {                        //dorysowanie dzioba
                line.setCharAt((size * size - 1), engineAndTip);
            }
        }
        return line.toString();
    }

    //podmienia znaki rysujac silniki i tarcze dla prawej czesci statku
    private String ShieldBot(byte i, StringBuilder line) {
        if (isShieldON) {
            line.setCharAt(0, engineAndTip);
            if (i != 2) {
                for (byte z = 0; z < size; z++) {
                    line.setCharAt(z * size + i - 2, shield2);
                }
            } else {
                for (byte z = 1; z < size; z++) {
                    line.setCharAt(z * size + i - 2, shield2);
                }
            }
        }
        return line.toString();
    }

    private void drawRocket() {
        for (byte i = 0; i < size; i++) {
            wing.append(star);
            System.out.print(ShieldTop(i, drawLine(drawTopWing(wing), line)) + "\n");
            line.setLength(0);               //ustawia singleLine na zero, by petla i mogla zaczac rysowanie od nowa
            wing.setLength(i + 1);           //ustawia wing na i+1 by wing zawieralo same star bez space
            countOfSpaces--;                 //zmniejsza ilosc dodawanych space
        }

        countOfSpaces = 1;
        wing.setLength(size - 1);

        for (byte i = size; i > 1; i--) {
            System.out.print(ShieldBot(i, drawLine(drawBotWing(wing), line)) + "\n");
            line.setLength(0);
            wing.setLength(i - 2);           //ustawia wing na i-2 by obcianac kolejne star z winga
            countOfSpaces++;
        }
    }


    public static void main(String[] args) {
        Kosmolot rocket = new Kosmolot(args);

        //validacje
        rocket.checkValues();
        rocket.checkChar();

        //rysowanie
        rocket.drawRocket();

    }
}