/*
Programy są trzy. Można za nie otrzymać odpowiednio do 50, 50 i 25(75) punktów. Samodzielnie zdecyduj, które/ile programów chcesz napisać. Jeśli wybierzesz mniej niż jeden, nie będziemy mogli zaproponować Ci udziału w HoT#3 ;-)

        Wracając do zadań:
        1. Równoległobok (maksymalnie 50 punktów)

        Narysuj (wypisz) na standardowym wyjściu równoległobok z gwiazdek.
        Program powinien przyjmować jako parametry z command line oddzielone spacjami kolejno następujące wartości:
        szerokość równoległoboku (bok a)
        wysokość równoległoboku (bok b)
        "kopnięcie"

        Wymagania dodatkowe:
        * każdy z boków powinien mieć długość większą od zera, nie większą od 100
        * "kopnięcie" może przyjmować wartości od -100 do 100 włącznie
        * w przypadku błędnych danych wejściowych program nie powinien nic wypisywać na standardowym wyjściu. Nic.
        * program zawsze powinien zakończyć się z kodem 0
        * czas działania programu nie powinien przekraczać 30 sekund. Jeśli program będzie wykonywał się dłużej niż 30 sekund, zostanie przerwany przez system sprawdzający i nie będzie punktu za ten przypadek testowy
        * ostatni wiersz zawierający znaki rozwiązania powinien kończyć się znakiem przejścia do kolejnej linii i to powinien być ostatni znak wypisywany przez program
        * plik z programem powinien nazywać się Rownoleglobok.java

        Przykładowe wyniki działania programu dla parametrów: 5 5 1 oraz dla parametrów: 5 5 -2 znajdziesz w załączonym pliku rownoleglobok.png
*/


public class Rownoleglobok {
    private static final byte NUMBEROFVALUES = 3;
    private byte width;
    private byte height;
    private byte push;


    private Rownoleglobok(String[] parts) {
        if (parts.length != NUMBEROFVALUES) {
            System.exit(0);
        }
        try {
            height = Byte.parseByte(parts[0]);
            width = Byte.parseByte(parts[1]);
            push = Byte.parseByte(parts[2]);
        } catch (NumberFormatException e) {
            System.exit(0);
        }
    }

    //sprawdza czy wartosci mieszcza sie w podanych zakresach
    private void checkValues() {
        if (!(width > 0 && width < 101) || !(height > 0 && height < 101) || !(push > -101 && push < 101)) {
            System.exit(0);
        }
    }

    //rysuje pojedyncza linie rownolegloboku
    private String drawLine() {
        StringBuilder sBLine = new StringBuilder();
        for (byte i = 0; i < width; i++) {
            sBLine.append("*");
        }
        return sBLine.toString();
    }

    //rysuje pojedyncze "kopniecie"
    private String drawSpace() {
        StringBuilder sBSpace = new StringBuilder();
        for (byte i = 0; i < Math.abs(push); i++) {
            sBSpace.append(" ");
        }
        return sBSpace.toString();
    }

    //rysuje rownoleglbok z linii i spacji
    private void drawRhomboid(String line, String space) {
        String sumOfSpaces = "";
        StringBuilder sBSumOfSpaces = new StringBuilder();

        for (byte i = height; i > 0; i--) {
            if (push >= 0) {
                System.out.print(sumOfSpaces + line + "\n");
                sBSumOfSpaces.append(space);
                sumOfSpaces = sBSumOfSpaces.toString();
            } else {
                //dla kopniecia minusowego
                for (byte k = i; k > 1; k--) {
                    sBSumOfSpaces.append(space);
                    sumOfSpaces = sBSumOfSpaces.toString();
                }
                System.out.print(sumOfSpaces + line + "\n");
                sumOfSpaces = "";
                sBSumOfSpaces.setLength(0);
            }
        }
    }

    public static void main(String[] args) {
        Rownoleglobok rhomboid = new Rownoleglobok(args);

        //walidacje
        rhomboid.checkValues();

        //rysowanie
        rhomboid.drawRhomboid(rhomboid.drawLine(), rhomboid.drawSpace());
    }
}